<?php

// đường dẫn ảo => đường dẫn thật
$routes['default_controller'] = 'home';
$routes['default_folder'] = 'home';
$routes['cron'] = 'cron/cron/index';
$routes['get-data-with-date'] = 'cron/cron/getDataWithDate';

//userr
$routes['login'] = 'user/user/login';
$routes['register'] = 'user/user/register';
$routes['profile'] = 'user/user/getProfile';
$routes['updateProfile'] = 'user/user/updateProfile';
$routes['changePassword'] = 'user/user/changePass';
$routes['bank-update'] = 'user/user/bankUpdate';
$routes['bankHistory'] = 'user/user/bankHistory';
$routes['get-history'] = 'user/user/getHistory';
$routes['detail'] = 'user/user/getDetail';
$routes['withdrawal'] = 'user/user/reqWithDrawl';
$routes['get-packages'] = 'user/vas/getPackages';
$routes['get-bank-list'] = 'user/vas/getBankList';

//check
$routes['check-list'] = 'user/vas/checkList';
$routes['check-one'] = 'user/vas/checkOne';

//admin
$routes['admin/get-users'] = 'admin/admin/getUserList';
$routes['admin/update-permission'] = 'admin/admin/updatePermission';
$routes['admin/reset-password'] = 'admin/admin/resetPass';

$routes['admin/confirm-transfer'] = 'admin/admin/confirmTransfer';
$routes['admin/get-list-request-transfer'] = 'admin/admin/getListResTransfer';
$routes['admin/get-list-transferred'] = 'admin/admin/getListTransferSuccess';

$routes['admin/get-package'] = 'admin/admin/getPackageList';
$routes['admin/update-package'] = 'admin/admin/updatePackage';
$routes['admin/create-package'] = 'admin/admin/storePackage';


$routes['admin/chart'] = 'admin/admin/chart';

$routes['admin/setting'] = 'admin/admin/getSetting';
$routes['admin/update-setting'] = 'admin/admin/editSetting';
