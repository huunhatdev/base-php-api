<?php
define('_DIR_ROOT', __DIR__);
require_once "configs/routes.php";
require_once "configs/app.php";
require_once "core/Routes.php";
require_once "core/Connection.php";
require_once "core/DataBase.php";
require_once "core/Auth.php";
require_once "core/RestFulAPI.php";
require_once "core/Telegram.php";
require_once "app/App.php";
require_once "core/Controller.php";
