<?php
class Controller
{
    public function my_model($my_model)
    {
        $path = "app/models/" . $my_model . ".php";
        if (file_exists($path)) {
            require_once $path;
            if (class_exists($my_model)) {
                $my_model = new $my_model();
                return $my_model;
            }
        }
        return false;
    }

    public function get($param)
    {
        if (isset($_GET[$param])) {
            return $_GET[$param];
        } else {
            return false;
        }
    }

    public function post($param)
    {
        $post = json_decode(file_get_contents('php://input'));
        if (!empty($post->$param)) {
            return trim($post->$param);
        } else {
            return false;
        }
    }
}
