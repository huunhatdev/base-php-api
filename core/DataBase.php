<?php
class DB
{
    // public static $DB_username = "root";
    // public static $DB_pass = "mysql";
    // public static $DB_local = "localhost";
    // public static $DB_db = "vasonline";
    public static $DB_username = "baogiamo_vasonline";
    public static $DB_pass = "nhat6069";
    public static $DB_local = "localhost";
    public static $DB_db = "baogiamo_vasonline";

    public static function connect()
    {
        $conn = mysqli_connect(DB::$DB_local, DB::$DB_username, DB::$DB_pass, DB::$DB_db);
        mysqli_set_charset($conn, 'UTF8');
        return $conn;
    }

    public static function fetchArray($data)
    {
        $res = array();
        while ($temp = mysqli_fetch_assoc($data)) {
            array_push($res, $temp);
        }
        return $res;
    }

    public static function affect($conn)
    {
        $affect_row = mysqli_affected_rows($conn);
        if ($affect_row)
            return true;
        return false;
    }
    public static function value($data, $value)
    {
        if (!$data) {
            return null;
        }
        $res = mysqli_fetch_assoc($data);
        if (isset($res[$value]))
            return $res[$value];
        return null;
    }
}
