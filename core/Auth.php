<?php
class Auth
{
    public function __construct()
    {
        require_once "app/models/UserModel.php";
    }
    public static function client()
    {
        $model = new UserModel();
        $headers = apache_request_headers();
        if (!empty($headers['token'])) {
            $getUser = $model->getDataUserApikey($headers['token']);
            if ($getUser) {
                $data['userID'] = $getUser['id'];
                $data['username'] = $getUser['username'];
                $data['isUser'] = $getUser['permission'] != "0" ? true : false;
                return $data;
            }
        }
        return false;
    }
    public static function admin()
    {
        require_once "app/models/UserModel.php";
        $model = new UserModel();
        $headers = apache_request_headers();
        if (!empty($headers['token'])) {
            $getUser = $model->getDataUserApikey($headers['token']);
            if ($getUser) {
                $data['userID'] = $getUser['id'];
                $data['isAdmin'] = $getUser['permission'] == "2" ? true : false;
                return $data;
            }
        }
        return false;
    }
}
