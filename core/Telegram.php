<?php
class Telegram
{
    private static $apikey = '5092049089:AAFwXrOyHRhyRG505HWKdZ5CF09y20ysHw8';
    private static $Telegram_url = 'https://api.telegram.org/bot';
    private static $chatId = '-1001614619485';


    public static function sendMsg($msg)
    {
        $curl = curl_init();
        $body = array(
            "chat_id"       => Telegram::$chatId,
            "text"          => $msg,
            "parse_mode"    => "html",
        );
        $body_html = http_build_query($body);
        $url = Telegram::$Telegram_url . Telegram::$apikey . '/sendMessage?' . $body_html;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_POSTFIELDS => array(),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
}
