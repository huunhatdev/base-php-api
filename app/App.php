<?php
class App
{
    private $__folder, $__controller, $__action, $__params, $__routes;
    function __construct()
    {
        global $routes;
        $this->__routes = new Routes();
        if (!empty($routes['default_controller'])) {
            $this->__controller = $routes['default_controller'];
            $this->__folder = $routes['default_folder'];
        }

        $this->__action = "index";
        $this->__params = [];
        $url = $this->handleUrl();
    }
    function getUrl()
    {
        if (!empty($_SERVER['PATH_INFO'])) {
            $url = $_SERVER['PATH_INFO'];
        } else {
            $url = '/';
        }
        return $url;
    }

    public function handleUrl()
    {
        $url = $this->getUrl();
        $url = $this->__routes->handleRoute($url);
        // echo $url;

        $urlArr = array_filter(explode('/', $url));
        $urlArr = array_values($urlArr);

        //xử lý fôlder
        if (!empty($urlArr[0])) {
            $this->__folder = ucfirst($urlArr[0]);
        } else {
            $this->__folder = ucfirst($this->__folder);
        }
        //xử lý controller
        if (!empty($urlArr[1])) {
            $this->__controller = ucfirst($urlArr[1]);
        } else {
            $this->__controller = ucfirst($this->__controller);
        }

        $pathFile = 'app/controllers/' . ($this->__folder) . '/' . ($this->__controller) . '.php';
        if (file_exists($pathFile)) {
            require_once $pathFile;
            if (class_exists($this->__controller)) {
                $this->__controller = new $this->__controller();
                unset($urlArr[0]);
                unset($urlArr[1]);
            } else
                $this->loadError();
        } else {
            $this->loadError();
        }

        //xử lý action
        if (!empty($urlArr[2])) {
            $this->__action = $urlArr[2];
            unset($urlArr[2]);
        } else {
            $this->loadError();
        }

        //xử lý params
        $this->__params = array_values($urlArr);

        //kiểm tra method
        if (method_exists($this->__controller, $this->__action)) {
            call_user_func_array([$this->__controller, $this->__action], $this->__params);
        } else {
            $this->loadError();
        }

        // //xử lý params
        // if (!empty($urlArr[2])) {
        //     $this->__params = $urlArr[2];
        // } else {
        //     $this->loadError();
        // }
    }

    public function loadError($name = '404')
    {
        require_once 'app/error/' . $name . '.php';
    }
}
