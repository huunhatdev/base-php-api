<?php
class UserModel
{
    public function storeData($id, $phone, $package, $chanel, $price, $rose, $date_time, $desc)
    {
        $sql = "INSERT INTO data_vas(`id`, `phone`, `package`, `chanel`, `price`, `rose`,`date_time`,  `desc`) 
        VALUES ('$id','$phone','$package','$chanel','$price','$rose','$date_time','$desc')";
        $conn = DB::connect();
        $query = $conn->query($sql);
        mysqli_close($conn);
        if ($query)
            return true;
        return false;
    }

    public function getDataUserApikey($apikey)
    {
        $sql = "SELECT id, username, name, phone, email, address, permission FROM data_user WHERE apikey = '$apikey' ";
        $conn = DB::connect();
        $result = $conn->query($sql);
        if (!mysqli_num_rows($result))
            return false;

        return mysqli_fetch_assoc($result);
    }

    public function storeDataUser($username, $password, $name, $email, $phone, $address)
    {
        $apikey = strtoupper(md5(uniqid()));
        $sql = "INSERT INTO data_user (username, password, name, email, phone, address,apikey) 
        VALUES ('$username', '$password', '$name', '$email', '$phone', '$address', '$apikey')";
        $conn = DB::connect();
        $result = $conn->query($sql);
        if ($result) {
            return true;
        }
        return false;
    }

    public function updateProfile($userID, $name, $email, $phone, $address)
    {
        $sql = "UPDATE data_user SET name = '$name', email = '$email', phone = '$phone', address = '$address' WHERE id = '$userID'";
        $conn = DB::connect();
        $conn->query($sql);
        $affect_row = mysqli_affected_rows($conn);
        if ($affect_row) {
            return true;
        }
        return false;
    }
    public function changePass($userID, $password)
    {
        $sql = "UPDATE data_user SET password = '$password' WHERE id = '$userID'";
        $conn = DB::connect();
        $conn->query($sql);
        $affect_row = mysqli_affected_rows($conn);
        if ($affect_row) {
            return true;
        }
        return false;
    }

    public function getDataUser($username, $password)
    {
        $sql = "SELECT id, username, name, phone, email, address, apikey FROM data_user WHERE username = '$username' AND password = '$password' ";
        $conn = DB::connect();
        $result = $conn->query($sql);
        if (!mysqli_num_rows($result))
            return false;

        return mysqli_fetch_assoc($result);
    }

    //bank
    public function bankUpdate($userID, $bankID, $accNum, $accName)
    {
        $conn = DB::connect();
        $sql = "UPDATE bank_user SET bankID = '$bankID', accNum = '$accNum' , accName = '$accName' WHERE userID = '$userID'";
        $conn->query($sql);
        $affect_row = mysqli_affected_rows($conn);
        if ($affect_row > 0)
            return true;
        $sql = "INSERT INTO bank_user (bankID, userID, accNum, accName) VALUES ('$bankID', '$userID', '$accNum', '$accName')";
        $res = $conn->query($sql);
        if ($res) {
            return true;
        }
        mysqli_close($conn);
        return false;
    }

    public function reqWithDrawl($userID, $money)
    {
        $conn = DB::connect();
        $sql = "SELECT SUM(rose) as sum  FROM vas_register WHERE userId = '$userID'";
        $total = mysqli_fetch_assoc($conn->query($sql))['sum'];
        $sql = "SELECT SUM(req_money) as withdrawn FROM withdrawal_request WHERE userId = '$userID' AND status <> '2'";
        $withdrawn = mysqli_fetch_assoc($conn->query($sql))['withdrawn'];
        $withdrawn = $withdrawn ? $withdrawn : 0;
        $left = $total - $withdrawn;
        if ($money < $left) {
            $sql = "INSERT INTO withdrawal_request (userId, req_money) VALUES ('$userID', '$money')";
            $result = $conn->query($sql);
            if ($result) {
                return true;
            }
        }
        return false;
    }

    public function bankHistory($userID)
    {
        $conn = DB::connect();
        $sql = "SELECT SUM(rose) as sum  FROM vas_register WHERE userId = '$userID'";
        $total = mysqli_fetch_assoc($conn->query($sql))['sum'];
        $sql = "SELECT SUM(req_money) as withdrawn FROM withdrawal_request WHERE userId = '$userID' AND status <> '2'";
        $withdrawn = mysqli_fetch_assoc($conn->query($sql))['withdrawn'];
        $withdrawn = $withdrawn ? $withdrawn : 0;
        $data['left'] = $total - $withdrawn;
        $sql = "SELECT bank_user.accNum , bank_user.accName,bank_list.bankName  FROM bank_user INNER JOIN bank_list ON bank_user.bankId = bank_list.bankID WHERE userId = '$userID'";
        $result = $conn->query($sql);
        $data['bankInfo'] = null;
        if (mysqli_num_rows($result)) {
            $temp = mysqli_fetch_assoc($result);
            $data['bankInfo'] = $temp;
        }
        $sql = "SELECT * FROM withdrawal_request WHERE userId = '$userID'";
        $result = $conn->query($sql);
        $data['history'] = array();
        while ($temp = mysqli_fetch_assoc($result)) {
            array_push($data['history'], $temp);
        }
        return $data;
    }

    public function checkVas($phone, $package, $userId)
    {
        $phone = preg_replace('/^(84|0)/', '', $phone);
        $package = strtoupper($package);

        $conn = DB::connect();

        //tax
        $sql = "SELECT tax FROM other WHERE id = 1";
        $tax = DB::value($conn->query($sql), "tax");

        //get vasID
        $sql = "SELECT data_vas.* , package.discount FROM data_vas INNER JOIN package ON data_vas.package = package.pack WHERE phone = '$phone' AND package = '$package' AND status = '0'";
        $result = $conn->query($sql);
        if (!mysqli_num_rows($result)) {
            mysqli_close($conn);
            $data['message'] = "Số điện thoại không tồn tại hoặc đã xác thực trên hệ thống";
            $data['isSuccess'] = false;
            return $data;
        }

        $res = mysqli_fetch_assoc($result);
        $vasId = $res['id'];
        $roseRevice = round((1 - $res['discount']) * $tax  * $res['rose'], 0, PHP_ROUND_HALF_DOWN);
        //update    
        $sql = "UPDATE data_vas SET status = 1 WHERE id = '$vasId'";
        $conn->query($sql);

        $sql = "INSERT INTO vas_register (vasId, userId, rose) VALUES ('$vasId', '$userId', '$roseRevice')";
        $conn->query($sql);

        $affect_row = mysqli_affected_rows($conn);
        mysqli_close($conn);
        if ($affect_row) {
            $data['message'] = "Xác thực thành công";
            $data['isSuccess'] = true;
            return $data;
        }
        return false;
    }

    public function getHistory($userID, $fromDate, $toDate)
    {
        $conn = DB::connect();
        $sql = "SELECT data_vas.id ,data_vas.phone, data_vas.package, data_vas.chanel, data_vas.price, vas_register.rose, data_vas.date_time 
        FROM vas_register INNER JOIN data_vas ON data_vas.id = vas_register.vasId 
        WHERE userId = '$userID' AND date_time BETWEEN '$fromDate' AND '$toDate'";
        $result = $conn->query($sql);
        $arr = DB::fetchArray($result);
        return $arr;
    }


    public function getDetail($userID)
    {
        $conn = DB::connect();
        $sql = "SELECT SUM(rose) as sum , COUNT(id) as count FROM vas_register WHERE userId = '$userID'";
        $result = mysqli_fetch_assoc($conn->query($sql));

        $data['sum'] = $result['sum'] ? $result['sum'] : 0;
        $data['count'] = $result['count'];

        $sql = "SELECT SUM(req_money) as withdrawn FROM withdrawal_request WHERE userId = '$userID' AND status = '1'";
        $result = mysqli_fetch_assoc($conn->query($sql));
        $data['withdrawn'] = $result['withdrawn'] ? $result['withdrawn'] : 0;

        $data['left'] = $data['sum'] - $data['withdrawn'];

        $sql = "SELECT MONTH(dateCheck) AS month,YEAR(dateCheck) AS year, SUM(rose) AS sum FROM vas_register  WHERE userId = '$userID' GROUP BY YEAR(dateCheck), MONTH(dateCheck)";
        $result = $conn->query($sql);

        $data['chart'] = array();
        while ($temp = mysqli_fetch_assoc($result)) {
            $temp['date'] = $temp['year'] . ' - ' . $temp['month'];
            array_push($data['chart'], $temp);
        }
        return $data;
    }

    public function getPackages()
    {
        $conn = DB::connect();

        $sql = "SELECT * FROM other WHERE id = 1";
        $tax = DB::value($conn->query($sql), "tax");
        $sms = DB::value($conn->query($sql), "sms");

        $sql = "SELECT * FROM package";
        $data = array();
        $result = $conn->query($sql);
        while ($temp = mysqli_fetch_assoc($result)) {
            $rose = round($temp['roseRaw'] * $tax * (1 - $temp['discount']), 0, PHP_ROUND_HALF_DOWN);
            $array = array(
                "id" => $temp['id'] * 1,
                "pack" => $temp['pack'],
                "price" => $temp['price'] * 1,
                "cycle" => $temp['cycle'] * 1,
                "rose" => $rose,
            );
            array_push($data, $array);
        }
        $response['sms'] = $sms;
        $response['data'] = $data;

        return $response;
    }

    public function getBankList()
    {
        $conn = DB::connect();
        $sql = "SELECT * FROM bank_list";
        $data = DB::fetchArray($conn->query($sql));
        return $data;
    }

    public function getProfile($userID)
    {
        $conn = DB::connect();
        $sql = "SELECT * FROM data_user  WHERE id = '$userID'";
        $data['profile'] = mysqli_fetch_assoc($conn->query($sql));
        $sql = "SELECT * FROM bank_user  WHERE userId = '$userID'";
        $data['bank'] = mysqli_fetch_assoc($conn->query($sql));
        return $data;
    }
}
