<?php
class AdminModel
{
    public function getUserList()
    {
        $conn = DB::connect();
        $sql = "SELECT * FROM data_user";
        $result = $conn->query($sql);
        $res = DB::fetchArray($result);
        mysqli_close($conn);
        return $res;
    }
    public function updatePermission($userID, $permission)
    {
        $conn = DB::connect();
        $sql = "UPDATE data_user SET permission = '$permission' WHERE id = '$userID'";
        $conn->query($sql);
        $flag = DB::affect($conn);
        return $flag;
    }
    public function forgotPass($userID, $password)
    {
        $conn = DB::connect();
        $sql = "UPDATE data_user SET password = '$password' WHERE id = '$userID'";
        $conn->query($sql);
        $flag = DB::affect($conn);
        return $flag;
    }

    //bank
    public function confirmTransfer($withDrawalID, $stt, $msg)
    {
        $conn = DB::connect();
        $sql = "UPDATE withdrawal_request SET status = '$stt', msg = '$msg' WHERE id = '$withDrawalID'";
        $conn->query($sql);
        $flag = DB::affect($conn);
        return $flag;
    }
    public function getListResTransfer()
    {
        $conn = DB::connect();

        $sql = "SELECT data_user.username, b.* FROM  (SELECT bank_list.bankName , a.* FROM 
        (SELECT withdrawal_request.id, withdrawal_request.req_money, withdrawal_request.date_time, bank_user.accNum, bank_user.accName, bank_user.bankID ,bank_user.userID 
        FROM withdrawal_request INNER JOIN bank_user  ON withdrawal_request.userId = bank_user.userID WHERE withdrawal_request.status = 0) as a 
        INNER JOIN bank_list ON bank_list.bankID = a.bankID) as b  INNER JOIN data_user ON data_user.id = b.userID  ORDER BY b.id DESC";

        $res = DB::fetchArray($conn->query($sql));
        mysqli_close($conn);
        return $res;
    }
    public function getListTransferSuccess()
    {
        $conn = DB::connect();

        $sql = "SELECT bank_list.bankName , a.* FROM 
        (SELECT withdrawal_request.id, withdrawal_request.req_money, withdrawal_request.date_time, bank_user.accNum, bank_user.accName, bank_user.bankID 
        FROM withdrawal_request INNER JOIN bank_user  ON withdrawal_request.userId = bank_user.userID WHERE withdrawal_request.status = 1) as a 
        INNER JOIN bank_list ON bank_list.bankID = a.bankID ORDER BY a.id DESC";

        $res = DB::fetchArray($conn->query($sql));
        mysqli_close($conn);
        return $res;
    }

    //package
    public function getPackageList()
    {
        $conn = DB::connect();
        $sql = "SELECT * FROM package";
        $res = DB::fetchArray($conn->query($sql));
        mysqli_close($conn);
        return $res;
    }
    public function updatePackage($pack, $price, $cycle, $roseRaw, $discount)
    {
        $pack = strtoupper($pack);
        $conn = DB::connect();
        $sql = "UPDATE package SET price = '$price', cycle = '$cycle', roseRaw = '$roseRaw',  discount = '$discount'  WHERE pack = '$pack'";
        $conn->query($sql);
        $flag = DB::affect($conn);
        mysqli_close($conn);
        return $flag;
    }
    public function storePackage($pack, $price, $cycle, $roseRaw, $discount)
    {
        $pack = strtoupper($pack);
        $conn = DB::connect();
        $sql = "INSERT INTO `package`(`pack`, `price`, `cycle`, `roseRaw`, `discount`) VALUES ('$pack','$price','$cycle','$roseRaw','$discount')";
        $res =  $conn->query($sql);
        if ($res)
            return true;
        return false;
    }

    //chart
    public function getChart()
    {
        $conn = DB::connect();

        $sql = "SELECT tax FROM other WHERE id = 1";
        $tax = DB::value($conn->query($sql), "tax");

        $sql = "SELECT a.*, b.count_from_user, b.sum_from_user 
        FROM (SELECT  MONTH(date_time) AS month,YEAR(date_time) AS year, COUNT(id) as count_total, SUM(rose) as sum_total FROM `data_vas`
        GROUP BY YEAR(date_time), MONTH(date_time)) as a 
        INNER JOIN (SELECT  MONTH(dateCheck) AS month,YEAR(dateCheck) AS year, COUNT(id) as count_from_user, SUM(rose) as sum_from_user FROM vas_register 
        GROUP BY YEAR(dateCheck), MONTH(dateCheck)) as b 
        ON a.month = b.month AND a.year = b.year";

        $res = DB::fetchArray($conn->query($sql));
        foreach ($res as $key => $value) {
            $res[$key]['sum_total'] = round($res[$key]['sum_total'] * $tax, 0, PHP_ROUND_HALF_DOWN);
            $res[$key]['sum_from_user'] *=  1;
        }
        mysqli_close($conn);
        return $res;
    }

    //setting
    public function editSetting($tax, $sms, $notice)
    {
        $conn = DB::connect();
        $sql = "UPDATE package SET tax = '$tax', sms = '$sms', notice = '$notice'  WHERE id = '1'";
        $conn->query($sql);
        $flag = DB::affect($conn);
        mysqli_close($conn);
        return $flag;
    }
    public function getSetting()
    {
        $conn = DB::connect();
        $sql = "SELECT * FROM other";
        $res = DB::fetchArray($conn->query($sql));
        mysqli_close($conn);
        return $res;
    }
}
