<?php
class VasOnlineModel
{
    public $BASE_URL = 'http://vasonline.mshop.vn';
    public $username = null;
    public $password = null;
    public $cookie = null;
    public $vasOnline;

    public function __construct()
    {
        require_once "app/models/UserModel.php";
        $this->vasOnline =  new UserModel();
        $sessionCron = $this->getSessionCron();
        $this->username = $sessionCron['username'];
        $this->password = $sessionCron['password'];
        $this->cookie = $sessionCron['cookie'];
    }
    public function getCookie()
    {
        $ch = curl_init($this->BASE_URL . '/vasonline');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // get headers too with this line
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $result = curl_exec($ch);
        // get cookie
        // multi-cookie variant contributed by @Combuster in comments
        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $result, $matches);
        $cookies = array();
        foreach ($matches[1] as $item) {
            parse_str($item, $cookie);
            $cookies = array_merge($cookies, $cookie);
        }
        // var_dump($cookies);
        $cookieRes = str_replace("&", ";", http_build_query($cookies));
        $this->setSessionCron($cookieRes);
        return $cookieRes;
    }

    public function login()
    {
        $body = array(
            'action' => 'agent_login',
            'username' => $this->username,
            'password' => $this->password,

        );
        $url = $this->BASE_URL . '/vasonline/admin/user_login_ajax.jsp';

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => http_build_query($body),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded; charset=UTF-8',
                'Cookie: ' . $this->cookie
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    public function getData($from_date, $to_date)
    {
        require_once "app/models/UserModel.php";
        $body = array(
            'action' => 'trans_search',
            'channel_id' => 0,
            'from_date' => $from_date,
            'to_date' => $to_date,
            'msisdn' => '',
            'page_index' => 1,
            'page_size' => 100000,
            'service_id' => 0,
            'state' => 1,
            'sub_agent' => '',
        );
        $url = $this->BASE_URL . '/vasonline/admin/giaodich/trans_ajax.jsp';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => http_build_query($body),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded; charset=UTF-8',
                'Cookie: ' . $this->cookie
            ),
        ));

        $response = curl_exec($curl);
        if (strpos($response, 'Đăng nhập')) {
            return false;
        }
        curl_close($curl);
        // return  'Cookie: JSESSIONID=' . $this->JSESSIONID . '; ROUTEID=.RWHCM1';

        $dom = new DOMDocument;
        $dom->loadHTML($response);


        $tr     = $dom->getElementsByTagName('tr');
        $a = [];


        for ($i = 1; $i < count($tr); $i++) {
            $element1 = $tr[$i];


            $id             = $element1->getElementsByTagName('td')->item(1)->textContent;
            $phone          = $element1->getElementsByTagName('td')->item(2)->textContent;
            $package        = $element1->getElementsByTagName('td')->item(4)->textContent;
            $chanel         = $element1->getElementsByTagName('td')->item(5)->textContent;
            $price          = $element1->getElementsByTagName('td')->item(6)->textContent;
            $rose           = $element1->getElementsByTagName('td')->item(8)->textContent;
            $date_time      = $element1->getElementsByTagName('td')->item(9)->textContent;
            $date_time      = DateTime::createFromFormat("d/m/Y H:i", $date_time)->format('Y-m-d H:i');
            $desc           = $element1->getElementsByTagName('td')->item(12)->textContent;


            $phone = preg_replace('/^(84|0)/', '', $phone);             //replace 84, 0
            $price = str_replace(",", "", $price);                      //del ,
            $rose = str_replace(",", "", $rose);                        //del ,

            if ($id) {
                array_push($a, array(
                    "id"        => $id,
                    "phone"     => $phone,
                    "package"   => $package,
                    "chanel"    => $chanel,
                    "price"     => $price,
                    "rose"      => $rose,
                    "date_time" => $date_time,
                    "desc"      => $desc,
                ));
                (new UserModel())->storeData($id, $phone, $package, $chanel, $price, $rose, $date_time, $desc);
            }
        }
        return json_encode($a, JSON_PRETTY_PRINT);
    }

    private function getSessionCron()
    {
        $sql = "SELECT * FROM cron WHERE 1";
        $conn = DB::connect();
        $result = $conn->query($sql);
        if (!mysqli_num_rows($result))
            return null;

        return mysqli_fetch_assoc($result);
    }

    private function setSessionCron($sessionId)
    {
        $sql = "UPDATE cron SET cookie = '$sessionId' WHERE id = 1";

        $conn = DB::connect();
        $result = $conn->query($sql);
        $affect_row = mysqli_affected_rows($conn);
        mysqli_close($conn);
        if ($affect_row > 0)
            return true;
        return false;
    }
}
