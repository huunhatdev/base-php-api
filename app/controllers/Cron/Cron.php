<?php
class Cron extends Controller
{
    public $model;
    public function index()
    {
        IL1:
        $time = time();
        $fromDate = date("d/m/Y H:i", strtotime('-120 seconds', $time));
        $toDate = date("d/m/Y H:i", $time);
        $response = $this->my_model('VasOnlineModel')->getData($fromDate, $toDate);
        if (!$response) {
            $this->reLogin();
            goto IL1;
        } else
            print_r($response);
    }
    public function getCookie()
    {
        $vasOnline = $this->my_model('VasOnlineModel')->getCookie();
        $vasOnlines = $this->my_model('VasOnlineModel')->login();

        print_r($vasOnline);
        print_r($vasOnlines);
    }

    private function reLogin()
    {
        $this->my_model('VasOnlineModel')->getCookie();
        $this->my_model('VasOnlineModel')->login();
    }

    public function getDataWithDate()
    {
        IL2:
        $fromDate =  $this->post('fromDate');
        $toDate =  $this->post('toDate');
        if (!$fromDate || !$toDate)
            return RestFulAPI::sendResponse(400, '{"msg":"Điền đầy đủ thông tin"}');
        $response = $this->my_model('VasOnlineModel')->getData($fromDate, $toDate);
        if (!$response) {
            $this->reLogin();
            goto IL2;
        } else
            print_r($response);
    }
}
