<?php
require_once "addons/excel/SimpleXLSX.php";
require_once "app/controllers/User/User.php";

use Shuchkin\SimpleXLSX;

class Vas extends User
{

    public function checkList()
    {
        $res['data'] = "error";
        $code = 404;
        if (!$this->checkUser()) {
            return;
        }
        if (!count($_FILES)) {
            $res['message'] = "Chưa gửi file đính kèm";
            $code = 400;
            goto end;
        }
        $fileName = $_FILES['file']['tmp_name'];
        $xlsx = SimpleXLSX::parse($fileName);
        if ($xlsx  && $_FILES['file']['size'] > 0) {
            $succes = 0;
            $false = array();

            foreach ($xlsx->readRows() as $k => $r) {
                if ($k == 0) continue; // skip first row
                $phone = preg_replace('/^(84|0)/', '', $r[0]);
                $pack = mb_strtoupper($r[1], "utf8");
                $res = $this->model->checkVas($phone, $pack, $this->userID);
                if ($res['isSuccess']) {
                    $succes++;
                } else {
                    array_push($false, $phone . " : " . $res['message']);
                }
            }

            $result['success'] = $succes;
            $result['fail'] = count($false);
            $res['data'] = $result;
            $result['dataFail'] = implode("\r", $false);
            $code = 200;
            if (false) {
                $file = "bao_cao_vas.txt";
                $txt = fopen($file, "w") or die("Unable to open file!");
                fwrite($txt, implode("\r", $false));
                fclose($txt);

                header('Content-Description: File Transfer');
                header('Content-Disposition: attachment; filename=' . basename($file));
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                header("Content-Type: text/plain");
                readfile($file);
            }
        } else {
            echo SimpleXLSX::parseError();
        }

        end:

        return RestFulAPI::sendResponse($code, json_encode($result));
    }

    public function checkOne()
    {
        if (!$this->checkUser()) {
            return;
        }
        $phone = $this->post('phone');
        $pack = $this->post('pack');
        if (!$phone || !$pack) {
            return RestFulAPI::sendMessage(400, 'Điền đầy đủ thông tin');
        }
        $res = $this->model->checkVas($phone, $pack, $this->userID);
        return RestFulAPI::sendResponse(200, json_encode($res));
    }

    public function getPackages()
    {
        $res =  $this->model->getPackages();
        return RestFulAPI::sendResponse(200, json_encode($res));
    }

    public function getBankList()
    {
        $res = $this->model->getBankList();
        return RestFulAPI::sendResponse(200, json_encode($res));
    }
}
