<?php
class User extends Controller
{
    public $model;
    public $userID = null;
    public $username = null;
    public $isBlock = false;

    public function __construct()
    {
        $this->model = $this->my_model('UserModel');
        $dataUser = Auth::client();
        if ($dataUser) {
            $this->userID = $dataUser['userID'];
            $this->username = $dataUser['username'];
            $this->isBlock = !$dataUser['isUser'];
        }
    }

    public function login()
    {
        $username = $this->post('username');
        $password = md5($this->post('password'));
        if (!$username || !$password) {
            return RestFulAPI::sendMessage(400, 'Điền đầy đủ thông tin');
        } else {

            $res = $this->model->getDataUser($username, $password);
            if ($res) {
                $data['data'] = $res;
                return RestFulAPI::sendResponse(200, json_encode($data));
            }
            return RestFulAPI::sendMessage(400, 'Tên đăng nhập hoặc mật khẩu không chính xác');
        }
    }

    public function register()
    {
        $username = $this->post('username');
        $password = md5($this->post('password'));
        $email = $this->post('email');
        $phone = $this->post('phone');
        $name = $this->post('name');
        $address = $this->post('address');
        if (!$username || !$password || !$email || !$phone || !$name || !$address) {
            return RestFulAPI::sendMessage(400, 'Điền đầy đủ thông tin');
        } else {

            $response = $this->model->storeDataUser($username, $password, $name, $email, $phone, $address);
            if ($response) {
                return RestFulAPI::sendMessage(200, 'Đăng kí tài khoản thành công');
            }
            return RestFulAPI::sendMessage(400, 'Tài khoản hoặc email đã được đăng kí');
        }
    }

    public function updateProfile()
    {
        if (!$this->checkUser())
            return;
        $name = $this->post('name');
        $email = $this->post('email');
        $phone = $this->post('phone');
        $address = $this->post('address');
        if (!$name || !$email || !$phone || !$address)
            return RestFulAPI::sendMessage(400, 'Điền đầy đủ thông tin');

        $userID = $this->userID;
        $res = $this->model->updateProfile($userID, $name, $email, $phone, $address);
        if ($res) {
            return  RestFulAPI::sendMessage(200, 'Cập nhật thông tin thành công');
        } else {
            return  RestFulAPI::sendMessage(400, 'Cập nhật thông tin thất bại');
        }
    }

    public function changePass()
    {
        if (!$this->checkUser())
            return;
        $password =  $this->post('password');
        if (!$password)
            return RestFulAPI::sendMessage(400, 'Điền đầy đủ thông tin');

        $userID = $this->userID;
        $res = $this->model->changePass($userID, $password);
        if ($res) {
            return  RestFulAPI::sendMessage(200, 'Đổi mật khẩu thành công');
        } else {
            return  RestFulAPI::sendMessage(400, 'Đổi mật khẩu thất bại');
        }
    }

    public function bankUpdate()
    {
        if (!$this->checkUser())
            return;

        $bankID = $this->post('bankID');
        $accNum = $this->post('accNum');
        $accName = $this->post('accName');
        if (!$bankID || !$accNum || !$accName) {
            return RestFulAPI::sendMessage(400, 'Điền đầy đủ thông tin');
        } else {
            $userID = $this->userID;
            $res = $this->model->bankUpdate($userID, $bankID, $accNum, strtoupper($accName));
            if ($res) {
                return  RestFulAPI::sendMessage(200, 'Cập nhật thông tin tài khoản ngân hàng thành công');
            } else {
                return  RestFulAPI::sendMessage(400, 'Cập nhật thông tin tài khoản ngân hàng thất bại');
            }
        }
    }

    public function bankHistory()
    {
        if (!$this->checkUser())
            return;
        $userID = $this->userID;
        $res = $this->model->bankHistory($userID);
        return  RestFulAPI::sendResponse(200, json_encode($res));
    }

    public function reqWithDrawl()
    {
        if (!$this->checkUser()) {
            return;
        }
        $money = $this->post('money');
        if (!$money) {
            return RestFulAPI::sendMessage(400, 'Điền đầy đủ thông tin');
        }
        if ($money < 50000) {
            return RestFulAPI::sendMessage(400, 'Số tiền không được nhỏ hơn 50.000đ');
        }
        $res = $this->model->reqWithDrawl($this->userID, $money);
        if ($res) {
            $date = date("d/m/Y H:i");
            $text = $this->username . ' đã yêu cầu rút ' . $money . ' - ' . $date;
            Telegram::sendMsg($text);
            return  RestFulAPI::sendMessage(200, 'Yêu cầu rút tiền thành công');
        }
        return  RestFulAPI::sendMessage(400, 'Xảy ra lỗi khi yêu cầu rút tiền');
    }

    public function getBankUser()
    {
        if (!$this->checkUser()) {
            return;
        }
    }

    public function getHistory()
    {
        if (!$this->checkUser()) {
            return;
        }

        $fromDate = $this->post('fromDate');
        $toDate = $this->post('toDate');
        if (!$fromDate || !$toDate) {
            return RestFulAPI::sendMessage(400, 'Điền đầy đủ thông tin');
        }
        $fromDate = DateTime::createFromFormat("d/m/Y H:i",  $fromDate)->format('Y-m-d H:i');
        $toDate = DateTime::createFromFormat("d/m/Y H:i",  $toDate)->format('Y-m-d H:i');
        $userID = $this->userID;
        $res = $this->model->getHistory($userID, $fromDate, $toDate);
        return RestFulAPI::sendResponse(200, json_encode($res));
    }

    public function getDetail()
    {
        if (!$this->checkUser()) {
            return;
        }
        $res['data'] = $this->model->getDetail($this->userID);
        return RestFulAPI::sendResponse(200, json_encode($res));
    }

    public function getProfile()
    {
        if (!$this->checkUser()) {
            return;
        }
        $res = $this->model->getProfile($this->userID);
        return RestFulAPI::sendResponse(200, json_encode($res));
    }

    public function checkUser()
    {
        if ($this->isBlock) {
            RestFulAPI::sendMessage(400, 'Tài khoản chưa được kích hoạt');
            return false;
        }
        if ($this->userID) {
            return true;
        } else {
            RestFulAPI::sendMessage(401, 'Không đủ quyền truy cập');
            return false;
        }
    }
}
