<?php
class Admin extends Controller
{
    public $model;
    public $userID = null;
    public $isAdmin = false;

    public function __construct()
    {
        $this->model = $this->my_model('AdminModel');
        $dataUser = Auth::admin();
        if ($dataUser) {
            $this->userID = $dataUser['userID'];
            $this->isAdmin = $dataUser['isAdmin'];
        }
    }
    public function getUserList()
    {
        if (!$this->checkAdmin())
            return;
        $res = $this->model->getUserList();
        return RestFulAPI::sendResponse(200, json_encode($res));
    }
    public function updatePermission()
    {
        if (!$this->checkAdmin())
            return;
        $userID = $this->post('userId');
        $permisson = $this->post('permisson');
        if (!$userID || !$permisson) {
            return RestFulAPI::sendMessage(400, 'Điền đầy đủ thông tin');
        }
        $res = $this->model->updatePermission($userID, $permisson);
        if ($res)
            return RestFulAPI::sendMessage(200, 'Thay đổi thành công');
        return RestFulAPI::sendMessage(400, 'Thay đổi thất bại');
    }
    public function resetPass()
    {
        if (!$this->checkAdmin())
            return;
        $userID = $this->post('userId');
        if (!$userID) {
            return RestFulAPI::sendMessage(400, "Điền đầy đủ thông tin");
        }
        $password = "12345678";
        $res = $this->model->updatePermission($userID, md5($password));
        if ($res)
            return RestFulAPI::sendMessage(200, 'Thay đổi thành công');
        return RestFulAPI::sendMessage(400, 'Thay đổi thất bại');
    }

    //bank
    public function confirmTransfer()
    {
        if (!$this->checkAdmin())
            return;
        $withDrawalID = $this->post('withDrawalID');
        $stt = $this->post('status');
        $msg = $this->post('msg');
        if (!$withDrawalID || !$stt || !$msg) {
            return RestFulAPI::sendMessage(400, "Điền đầy đủ thông tin");
        }
        $res = $this->model->confirmTransfer($withDrawalID, $stt, $msg);
        if ($res)
            return RestFulAPI::sendMessage(200, 'Thực hiện thành công');
        return RestFulAPI::sendMessage(400, 'Thực hiện thất bại');
    }
    public function getListResTransfer()
    {
        if (!$this->checkAdmin())
            return;
        $res = $this->model->getListResTransfer();
        return RestFulAPI::sendResponse(200, json_encode($res));
    }
    public function getListTransferSuccess()
    {
        if (!$this->checkAdmin())
            return;
        $res = $this->model->getListTransferSuccess();
        return RestFulAPI::sendResponse(200, json_encode($res));
    }

    //package
    public function getPackageList()
    {
        if (!$this->checkAdmin())
            return;
        $res = $this->model->getPackageList();
        return RestFulAPI::sendResponse(200, json_encode($res));
    }
    public function updatePackage()
    {
        if (!$this->checkAdmin())
            return;
        $pack = $this->post('pack');
        $price = $this->post('price');
        $cycle = $this->post('cycle');
        $roseRaw = $this->post('roseRaw');
        $discount = $this->post('discount');
        if (!$pack || $price * 1 < 0 || $cycle * 1 < 0 || $roseRaw * 1 < 0 || !$discount) {
            return RestFulAPI::sendMessage(400, "Điền đầy đủ thông tin");
        }
        $res = $this->model->updatePackage($pack, $price, $cycle, $roseRaw, $discount);
        if ($res)
            return RestFulAPI::sendMessage(200, 'Cập nhật thành công');
        return RestFulAPI::sendMessage(400, 'Cập nhật thất bại');
    }
    public function storePackage()
    {
        if (!$this->checkAdmin())
            return;
        $pack = $this->post('pack');
        $price = $this->post('price');
        $cycle = $this->post('cycle');
        $roseRaw = $this->post('roseRaw');
        $discount = $this->post('discount');
        if (!$pack || $price * 1 < 0 || $cycle * 1 < 0 || $roseRaw * 1 < 0 || !$discount) {
            return RestFulAPI::sendMessage(400, "Điền đầy đủ thông tin");
        }
        $res = $this->model->storePackage($pack, $price, $cycle, $roseRaw, $discount);
        if ($res)
            return RestFulAPI::sendMessage(200, 'Thêm gói thành công');
        return RestFulAPI::sendMessage(400, 'Thêm gói thất bại');
    }


    //chart
    public function chart()
    {
        if (!$this->checkAdmin())
            return;
        $res = $this->model->getChart();
        return RestFulAPI::sendResponse(200, json_encode($res));
    }

    //setting
    public function getSetting()
    {
        if (!$this->checkAdmin())
            return;
        $res = $this->model->getSetting();
        return RestFulAPI::sendResponse(200, json_encode($res));
    }

    public function editSetting()
    {
        if (!$this->checkAdmin())
            return;
        $tax = $this->post('tax');
        $sms = $this->post('sms');
        $notice = $this->post('notice');
        if (!$tax  || !$sms || !$notice) {
            return RestFulAPI::sendMessage(400, "Điền đầy đủ thông tin");
        }
        $res = $this->model->editSetting($tax, $sms, $notice);
        if ($res)
            return RestFulAPI::sendMessage(200, 'Cập nhật thành công');
        return RestFulAPI::sendMessage(400, 'Cập nhật thất bại');
    }


    public function checkAdmin()
    {
        if (!$this->isAdmin) {
            RestFulAPI::sendMessage(401, "Không đủ quyền truy cập");
            return false;
        }
        if ($this->userID) {
            return true;
        } else {
            RestFulAPI::sendMessage(401, "Không đủ quyền truy cập");
            return false;
        }
    }
}
